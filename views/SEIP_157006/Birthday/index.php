<?php
require_once("../../../vendor/autoload.php");

$objBirthday = new \App\Birthday\Birthday();
$allData = $objBirthday-> index();






use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday View</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #31b0d5;
            color: #2b669a;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>
<div class="container bg-color">
<h1>Birthday</h1>
   <h4 style="float: right"> <a href='trashed.php' class='btn btn-lg btn-default' > Go To Trashed List</a></h4>
<table cellspacing="0px" class="table table-stried">
    <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>Name</th>
        <th>Birthday</th>
        <th>Action Buttons</th>
    </tr>
    <?php
    $serial = 1;
    foreach($allData as $oneData){
        if($serial%2){
            $bgColor = '#cccccc';
        }
        else
            $bgColor = '#ffffff';
        echo "
                <tr style='background-color: $bgColor'>
                    <td>$serial</td>
                    <td>$oneData->user_id</td>
                    <td>$oneData->user_name</td>
                    <td>$oneData->birthday</td>
                    <td><a href='view.php?id=$oneData->user_id' class='btn btn-info'>View</a>
                    <a href='edit.php?id=$oneData->user_id' class='btn btn-primary'>Update</a>
                    <a href='trash.php?id=$oneData->user_id' class='btn btn-warning'>Soft Delete</a>
                    <a href='delete.php?id=$oneData->user_id' class='btn btn-danger'>Delete</a></td>

                </tr>
            ";
        $serial++;


    }


    ?>
</table>
    </div>
</body>
</html>