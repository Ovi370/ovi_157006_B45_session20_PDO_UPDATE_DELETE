<?php
require_once("../../../vendor/autoload.php");
$objBirthday = new \App\Birthday\Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Birthday</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/style.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #dcdcdc;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>

<div class="container bg-color">
<form action="update.php" method="post">

    <h4>Edit Name:</h4>
    <input type="text" name="user_name" value="<?php echo $oneData->user_name ?>">
    <h4>Edit Birthday:</h4>
    <input type="date" name="birthday" value="<?php echo $oneData->birthday ?>">
    <input type="hidden" name="id" value="<?php echo $oneData->user_id ?>">
    <input type="submit">



</form>
</div>
</body>
</html>