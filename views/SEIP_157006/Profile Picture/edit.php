<?php
require_once("../../../vendor/autoload.php");
$objProPic = new \App\Profile_Picture\ProfilePicture();
$objProPic->setData($_GET);
$oneData = $objProPic->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Profile Picture</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/style.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #dcdcdc;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>

<div class="container bg-color">
    <form name="file" action="update.php" method="post" enctype="multipart/form-data">

        <h4>Edit Name:</h4>
        <input type="text" name="user_name" value="<?php echo $oneData->user_name ?>">
        <h4>Edit Profile Picture:</h4>
        <input type="file" id="file" name="file">
        <input type="hidden" name="id" value="<?php echo $oneData->user_id ?>">
        <input type="submit">



    </form>
</div>
</body>
</html>