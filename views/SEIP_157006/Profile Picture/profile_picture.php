<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upload Profile Picture</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #c0c0c0;
        }
        input{
            color: #000000;
        }

    </style>
</head>
<body>
<div class="container bg-color">
<form name="file" action="storeProfilePicture.php" method="post" enctype="multipart/form-data">
    <h4>Enter Name:</h4>
    <input type="text" name="user_name">
    <input type="file" id="file" name="file" >
    <input type="submit" class="btn btn-primary button">
</form>
</div>
</body>
</html>