<?php
require_once("../../../vendor/autoload.php");
$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$oneData = $objGender->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Gender</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/style.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #dcdcdc;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>

<div class="container bg-color">
    <form action="update.php" method="post">

        <h4>Edit Name:</h4>
        <input type="text" name="user_name" value="<?php echo $oneData->user_name ?>">
        <input type="text" value="<?php echo $oneData->gender ?>">
        <h4>Edit Gender:</h4>
        <input type="radio" name="gender" value="male">Male
        <input type="radio" name="gender" value="female">Female
        <input type="hidden" name="id" value="<?php echo $oneData->user_id ?>">
        <input type="submit">



    </form>
</div>
</body>
</html>