<?php

require_once("../../../vendor/autoload.php");
$objCity = new \App\City\City();
$objCity->setData($_GET);
$id = $_GET['id'];
$oneData = $objCity->view();



echo "
<div class='container bg-color'>
<table>
    <tr>
        <td>ID: </td>
        <td>$oneData->user_id</td>
    </tr>
    <tr>
        <td>Name: </td>
        <td>$oneData->user_name</td>
    </tr>
    <tr>
        <td>City: </td>
        <td>$oneData->city</td>
    </tr>
    <tr>
        <td>Post Code: </td>
        <td>$oneData->post_code</td>
    </tr>
    <tr>
        <td>Post Office: </td>
        <td>$oneData->post_office</td>
    </tr>
    <tr>
        <td>Police Station: </td>
        <td>$oneData->police_station</td>
    </tr>
    <tr>
        <td>Detail Address: </td>
        <td>$oneData->detail_address</td>
    </tr>

</table>
</div>
";


echo "
<div class='container bg-color'>
Are you sure to delete this item? <br>
 <a href='delete.php?Yes=1 & id=$id'  class='btn btn-danger'>Yes</a>
 <a href='index.php?' class='btn btn-warning'>No</a>
</div>
";

if(isset($_GET['Yes']) && $_GET['Yes']){

    $oneData= $objCity->delete();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Address View</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
        .bg-color{
            background-color: #31b0d5;
            color: #2b669a;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
</html>

