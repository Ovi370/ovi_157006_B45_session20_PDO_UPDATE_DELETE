<?php
require_once("../../../vendor/autoload.php");

$objCity = new \App\City\City();
$allData = $objCity-> trash();


use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trash List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #31b0d5;
            color: #31b0d5;
        }
        table{


            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>
<h1>Trashed List</h1>
<a href="index.php" class="btn btn-lg btn-default">Home</a>
<div class="container bg-color">
    <table cellspacing="0px" class="table table-stried">
        <tr>
            <th>Serial Number</th>
            <th>ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Post Code</th>
            <th>Post Office</th>
            <th>Police Station</th>
            <th>Detail Address</th>
        </tr>
        <?php
        $serial=1;
        foreach($allData as $oneData){
            if($serial%2){
                $bgColor = '#cccccc';
            }
            else
                $bgColor = '#ffffff';
            echo "
                <tr style='background-color: $bgColor'>
                    <td>$serial</td>
                    <td>$oneData->user_id</td>
                    <td>$oneData->user_name</td>
                    <td>$oneData->city</td>
                    <td>$oneData->post_code</td>
                    <td>$oneData->post_office</td>
                    <td>$oneData->police_station</td>
                    <td>$oneData->detail_address</td>
                    <td><a href='view.php?id=$oneData->user_id' class='btn btn-info'>View</a>
                    <a href='recover.php?id=$oneData->user_id' class='btn btn-info'>Recover</a></td>



                </tr>
            ";
            $serial++;

        }


        ?>
    </table>
</div>
</body>
</html>