<?php
require_once("../../../vendor/autoload.php");
$objHobbies = new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Hobbies</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/style.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #dcdcdc;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>

<div class="container bg-color">
    <form action="update.php" method="post">

        <h4>Enter The Name:</h4>
        <input type="text" name="user_name" value="<?php echo $oneData->user_name ?>">
        <h4>Hobbies:</h4>
        <input type="checkbox" name="hobbies1" value="gardening">gardening<br>
        <input type="checkbox" name="hobbies2" value="gammeing">gammeing<br>
        <input type="checkbox" name="hobbies3" value="photography">photography<br>
        <input type="checkbox" name="hobbies4" value="sports">sports<br>
        <input type="checkbox" name="hobbies5" value="programming">programming<br>
        <input type="checkbox" name="hobbies6" value="drawing">drawing<br>
        <input type="hidden" name="id" value="<?php echo $oneData->user_id ?>">
        <input type="submit" class="btn btn-primary button">



    </form>
</div>
</body>
</html>