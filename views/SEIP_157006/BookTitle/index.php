<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new \App\BookTitle\BookTitle();
$allData = $objBookTitle-> index();






use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #dcdcdc;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>
<h1>Book Active List</h1>
<a href="trashed.php" class="btn btn-lg btn-default">Trashed</a>
<table cellspacing="0px" class="table table-stried">
    <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>Book Name</th>
        <th>Author Name</th>
        <th>Action Buttons</th>
    </tr>
    <?php
    $serial=1;
        foreach($allData as $oneData){
            if($serial%2){
                $bgColor = '#cccccc';
            }
            else
                $bgColor = '#ffffff';
            echo "
                <tr style='background-color: $bgColor'>
                    <td>$serial</td>
                    <td>$oneData->book_id</td>
                    <td>$oneData->book_name</td>
                    <td>$oneData->author_name</td>
                    <td><a href='view.php?id=$oneData->book_id' class='btn btn-info'>View</a>
                    <a href='edit.php?id=$oneData->book_id' class='btn btn-primary'>Update</a>
                    <a href='trash.php?id=$oneData->book_id' class='btn btn-warning'>Soft Delete</a>
                    <a href='delete.php?id=$oneData->book_id' class='btn btn-danger'>Delete</a></td>


                </tr>
            ";
            $serial++;

        }


    ?>
</table>
</body>
</html>