<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new \App\BookTitle\BookTitle();
$allData = $objBookTitle-> trash();


use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trash List</title>
    <style>
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>
<h1>Book Trashed List</h1>
<a href="index.php" class="btn btn-lg btn-default">Home</a>
<table cellspacing="0px">
    <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>Book Name</th>
        <th>Author Name</th>
    </tr>
    <?php
    $serial=1;
    foreach($allData as $oneData){
        if($serial%2){
            $bgColor = '#cccccc';
        }
        else
            $bgColor = '#ffffff';
        echo "
                <tr style='background-color: $bgColor'>
                    <td>$serial</td>
                    <td>$oneData->book_id</td>
                    <td>$oneData->book_name</td>
                    <td>$oneData->author_name</td>
                    <td><a href='view.php?id=$oneData->book_id' class='btn btn-info'>View</a>
                    <a href='recover.php?id=$oneData->book_id' class='btn btn-info'>Recover</a></td>



                </tr>
            ";
        $serial++;

    }


    ?>
</table>
</body>
</html>