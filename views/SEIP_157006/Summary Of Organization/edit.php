<?php
require_once("../../../vendor/autoload.php");
$objSummary = new \App\Summary_Of_Organization\Summary_Of_Organization();
$objSummary->setData($_GET);
$oneData = $objSummary->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Summary of Organization</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/style.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #dcdcdc;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>

<div class="container bg-color">
    <form action="update.php" method="post">

        <h4>Edit Name:</h4>
        <input type="text" name="user_name" value="<?php echo $oneData->user_name ?>">
        <h4>Edit Organization Name:</h4>
        <input type="text" name="organization_name" value="<?php echo $oneData->organization_name ?>">
        <h4>Edit Summary Organization</h4>
        <input type="text" name="summary_of_organization" value="<?php $oneData->summary_of_organization?>">
        <input type="hidden" name="id" value="<?php echo $oneData->user_id ?>">
        <input type="submit">



    </form>
</div>
</body>
</html>